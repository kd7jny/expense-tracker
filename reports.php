<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/24/2018
 * Time: 8:41 AM
 */
require_once "startup.php";


$data = [];
$dir = 'reports';
$files = scandir($dir);
foreach($files as $filename):
    if(!in_array($filename, ['.', '..']) ):
        $id  =  basename($filename,'.php');
        $resource = include "reports/".$filename;
        if($resource):
            $title = $resource['title'];
            if(isset($resource['disabled']) && $resource['disabled'] == true):
                // do nothing
            else:
                $data[$title] = ['rid' => $id, 'title' => $title];
            endif;
        endif;
    endif;
endforeach;

ksort($data);
$tmparray = [];
foreach($data as $item){
    $tmparray[] = $item;
}
$data = $tmparray;

$smarty->assign('menu','report');
$smarty->assign('reports', $data);
$smarty->display('reports.tpl');
