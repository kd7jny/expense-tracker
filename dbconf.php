<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/24/2018
 * Time: 8:39 AM
 */

$file = __DIR__ ."/data/budget.sqlite";
$db = new PDO('sqlite:'.$file) or die("There was a problem accessing database. Please contact the admin.");
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

