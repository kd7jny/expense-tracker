{include file="header.tpl" }

<h3><a href="catadd.php" class="fas fa-plus" style="text-decoration: none; color:red;">Add Category</a></h3>

<h3>Active Categories</h3>
{nocache}
<table>
        <tr>
                <th>Category</th>
                <th>Budget Amt</th>
                <th>Shared</th>

        </tr>
    {section name=cat loop=$activeloop}
       <tr>
               <td><a href="catedit.php?cid={$activeloop[cat].cid}">{$activeloop[cat].category}</a></td>
               <td>${$activeloop[cat].amount|string_format:"%.2f"}</td>
               <td>{if ($activeloop[cat].shared == 1)} Yes {else} No {/if}</td>
       </tr>
    {sectionelse}
            <tr><td colspan="3">No Catgories Found</td></tr>
    {/section}
</table>
{/nocache}

<h3>Inactive Categories</h3>

{nocache}
    <table>
        <tr>
            <th>Category</th>
            <th>Shared</th>

        </tr>
        {section name=cat loop=$inactiveloop}
            <tr>
                <td><a href="catedit.php?cid={$inactiveloop[cat].cid}">{$inactiveloop[cat].category}</a></td>
                <td>{if ($inactiveloop[cat].shared == 1)} Yes {else} No {/if}</td>
            </tr>
            {sectionelse}
            <tr><td colspan="2">No Catgories Found</td></tr>
        {/section}
    </table>
{/nocache}

{include file="footer.tpl"}
