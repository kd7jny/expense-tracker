{include file="header.tpl" }
<h3>Update Category</h3>
{nocache}
    <form action="catedit.php?cid={$cid}" method="post" class="form_settings">
<table>
    {section name=cat loop=$loop}

    <tr><th>Category</th><td><input type="text" name="category" value="{$loop[cat].category}"></td></tr>
    <tr><th>Amount</th><td>$<input type="text" name="amount" value="{$loop[cat].amount|string_format:"%.2f"}"></td></tr>
        <tr><th>Notes</th><td><textarea name="notes" cols=22 rows=3>{$loop[cat].notes}</textarea></td></tr>
    <tr><th>Shared</th><td><input type="checkbox" value=1 name="shared" {if ($loop[cat].shared == 1)} checked="checked"{/if}></td></tr>
    <tr><th>Active</th><td><input type="checkbox" value=1 name="active" {if ($loop[cat].active == 1)} checked="checked"{/if}></td></tr>
        {sectionelse}
        <tr><td>No Catgories Found</td></tr>

    {/section}
</table>
    <input type="submit" value="Update" class="mysubmit">
    </form>
<div>{$errors}</div>
{/nocache}


{include file="footer.tpl"}
