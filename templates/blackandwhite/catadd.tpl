{include file="header.tpl" }
<h3>Add Category</h3>
{nocache}
    <form action="catadd.php" method="post" class="form_settings">
<table>

    <tr><th>Category</th><td><input type="text" name="category" value="{$data.category}"></td></tr>
    <tr><th>Amount</th><td>$<input type="number" step=0.01 name="amount" value="{$data.amount|string_format:"%.2f"}"></td></tr>
    <tr><th>Notes</th><td><textarea name="notes" cols=22 rows=3>{$data.notes}</textarea></td></tr>
    <tr><th>Shared</th><td><input type="checkbox" value=1 name="shared"></td></tr>

</table>
    <input type="submit" value="Add" class="mysubmit">
    </form>
<div>{$errors}</div>
{/nocache}


{include file="footer.tpl"}
