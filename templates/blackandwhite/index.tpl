{include file="header.tpl"}

<h3>Add Transaction</h3>
{nocache}
<form class="form_settings" action="addtransaction.php" method="POST">
<table>
    {*<tr><th>User</th><td>*}
            {*<select name="user">*}
                {*{section name=user loop=$users}*}
                    {*<option value="{$users[user].uid}" {if $users[user].userid == $uid} selected="selected" {/if}>{$users[user].name}</option>*}
                {*{/section}*}
            {*</select>*}
        {*</td></tr>*}
    <tr><th>Category</th><td>
            <select name="category">
                <option></option>
                {section name=cat loop=$category}
                    <option value="{$category[cat].cid}">{$category[cat].category} {if $category[cat].shared == 1}[shared]{/if}</option>
                {/section}
            </select>
        </td></tr>
    <tr><th>Description</th><td><input type="text" name="desciption"></td></tr>
    <tr><th>Notes</th><td><input type="text" name="notes"></td></tr>
    <tr><th>Amount</th><td>$<input type="number" step='0.01' name="amount" value="{$amount|string_format:"%.2f"}"></td></tr>
   <tr><th>Date</th><td><input type="date" name='tdate' value="{$date}"></td></tr>
</table>
    <input type="submit" value="Add" class="mysubmit">
</form>
{/nocache}
<br><hr><br>
<h3>Recent Transactions (Last 100)</h3>
<table>
    <tr>
        <th></th>
        <th>Date</th>
        <th>Category</th>
        {*<th>Description</th>*}
        <th>Amount</th>
    </tr>
    {nocache}
    {section name=trans loop=$transactions}


    <tr>
        <td><a href="edittransaction.php?tid={$transactions[trans].tid}"><i class="fas fa-pencil-alt"></i></a></td>
        <td>{$transactions[trans].realdate|date_format:"%D"}</td>
        <td>{$transactions[trans].category}{if $transactions[trans].description != ''} <br> <small>{$transactions[trans].description}</small>{/if}</td>
        {*<td>{$transactions[trans].description}</td>*}
        <td>${$transactions[trans].amount|string_format:"%.2f"}</td>

    </tr>
    {sectionelse}
        <tr><td colspan="4">No Recent Transactions</td></tr>
    {/section}{/nocache}

</table>
<!--
{nocache}
  <ul>
      {section name=user loop=$myuser}
        <li>{$smarty.section.user.index}</li>
        <li>Userid : {$myuser[user].userid}</li>
        <li>Email : {$myuser[user].email}</li>
        <li>Password Hash : {$myuser[user].passwordhash}</li>
        <li>Name : {$myuser[user].name}</li>
          {sectionelse}
        <li>No Users Found</li>
      {/section}
  </ul>
{if isset($session.user.name)} {$session.user.name} {/if}<br><br>
{/nocache}

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ac aliquet felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus vitae tortor ac neque molestie commodo. Maecenas sed euismod urna. Phasellus eu elit sit amet est tincidunt blandit in sed augue. Morbi semper, dui eget elementum condimentum, odio nunc consequat urna, et facilisis turpis tellus nec libero. Etiam egestas iaculis ligula, quis luctus velit ornare ut. In eu velit quam. Phasellus eu nisi eu erat egestas suscipit. Mauris fringilla ut eros vitae feugiat. Phasellus placerat vehicula dapibus. Nunc facilisis sapien ut neque fermentum fringilla. Morbi eleifend orci ac sapien condimentum semper. Aliquam tortor elit, lacinia nec massa eget, accumsan tempus nisi. Mauris pharetra, lorem eu laoreet bibendum, turpis sapien efficitur turpis, a cursus lacus mi at turpis.
-->
{include file="footer.tpl"}