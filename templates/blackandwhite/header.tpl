<!DOCTYPE HTML>
<html>

<head>
    <title>Expense Tracker</title>
    <meta name="description" content="Expense Tracker" />
    <meta name="keywords" content="website keywords, website keywords" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="apple-touch-icon" sizes="57x57" href="ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="ico/favicon-16x16.png">
    <link rel="manifest" href="ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="./templates/blackandwhite/style/style.css" title="style" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<script>
    function fnExcelReport(id)
    {
        console.log(id);
        var tab_text = '<table border="1px" style="font-size:20px" ">';
        var textRange;
        var j = 0;
        var tab = document.getElementById(id); // id of table
        var lines = tab.rows.length;

        // the first headline of the table
        if (lines > 0) {
            tab_text = tab_text + '<tr bgcolor="#DFDFDF">' + tab.rows[0].innerHTML + '</tr>';
        }

        // table data lines, loop starting from 1
        for (j = 1 ; j < lines; j++) {
            tab_text = tab_text + "<tr>" + tab.rows[j].innerHTML + "</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");             //remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi,"");                 // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, "");    // reomves input params
        // console.log(tab_text); // aktivate so see the result (press F12 in browser)

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        // if Internet Explorer
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            txtArea1.document.open("txt/html","replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "DataTableExport.xls");
        }
        else // other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);
    }
</script>
<body>
<div id="main">
    <div id="header">
        <div id="logo">
            <div id="logo_text">
                <!-- class="logo_colour", allows you to change the colour of the text -->
                <h1>Expense Tracker</h1>
                <h2></h2>
            </div>
        </div>
        <div id="menubar">
            <ul id="menu">
                {nocache}
                <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
                <li {if $menu == 'home' }class="selected" {/if}><a href="index.php" title="home" selected><i class="fas fa-home fa-3x"></i></a></li>
                <li {if $menu == 'cat' }class="selected" {/if}><a href="category.php" title="Categories"><i class="far fa-folder-open fa-3x"></i></a></li>
                <li {if $menu == 'report' }class="selected" {/if}><a href="reports.php" title="Reports"><i class="far fa-newspaper fa-3x"></i></a></li>
                <li {if $menu == 'user' }class="selected" {/if}><a href="user.php" title="User Info"><i class="fas fa-user fa-3x"></i></a></li>
                   {if $currentpage != 'login.php'}
                    <li><br>
                        <form action="changesession.php" method="post">
                         <span style="font: 150% Arial;Color: White;">User:</span>
                            <select name="currentuser" style="font: 150% Arial" onchange="this.form.submit();">
                                {section name=userindex loop=$users}
                                    <option value="{$users[userindex].userid}" {if $users[userindex].userid == $active_uid} selected="selected" {/if}>{$users[userindex].name}</option>
                                    {/section}
                            </select>
                            <input type="hidden" name="page" value="{$currentpage}">
                        </form>

                    </li>
                   {/if}
                {/nocache}
            </ul>
        </div>
    </div>
    <div id="site_content">
        {*<div class="sidebar">*}

        {*</div>*}
        <div id="content">