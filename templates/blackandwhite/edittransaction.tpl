{include file="header.tpl"}

<h3>Edit Transaction</h3>
{nocache}
<form class="form_settings" action="edittransaction.php?tid={$tid}" method="POST">
<table>
    <tr><th>Category</th><td>
            <select name="category">
                <option></option>
                {section name=cat loop=$category}
                    <option value="{$category[cat].cid}" {if $cat == $category[cat].cid} selected=selected {/if}>{$category[cat].category} {if $category[cat].shared == 1}[shared]{/if}</option>
                {/section}
            </select>
        </td></tr>
    <tr><th>Description</th><td><input type="text" name="description" value="{$description}"></td></tr>
    <tr><th>Notes</th><td><input type="text" name="notes" value="{$notes}"></td></tr>
    <tr><th>Amount</th><td>$<input type="number" step='0.01' name="amount" value="{$amount|string_format:"%.2f"}"></td></tr>
   <tr><th>Date</th><td><input type="date" name='tdate' value="{$date}"></td></tr>
    <tr><th>Delete</th><td><input type="checkbox" name='delete' value="1"></td></tr>
</table>
    <input type="submit" value="Update" class="mysubmit">
</form>
{/nocache}

{include file="footer.tpl"}