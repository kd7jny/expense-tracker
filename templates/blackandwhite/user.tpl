{include file="header.tpl" }

{nocache}

    <form action="user.php" method="post" class="form_settings">

        <table>
            {section name=cat loop=$loop}

                <tr><th>Name</th><td><input type="text" name="name" value="{$loop[cat].name}"><input type="hidden" name="userid" value="{$loop[cat].userid}"></td></tr>
                <tr><th>Email</th><td><input type="text" name="email" value="{$loop[cat].email}"></td></tr>
                <tr><td colspan="2">Enter new Password if you wish to change it</td></tr>
                <tr><th>Old Password</th><td><input type="password" name="oldpassword"></td></tr>
                <tr><th>New Password</th><td><input type="password" name="newpassword"></td></tr>
                <tr><th>Confirm New Password</th><td><input type="password" name="confirmpassword"></td></tr>
                {sectionelse}
                <tr><td>No user</td></tr>

            {/section}
        </table>
        <input type="submit" value="Update" class="mysubmit">
    </form>
    <div>
        {section name=error loop=$errors}
            {$errors[error]}
            {/section}
    </div>
    <div>

        <form action="logout.php" class="form_settings">
            <input type="submit" value="Logout" class="mysubmit">

        </form>

    </div>

{/nocache}


{include file="footer.tpl"}