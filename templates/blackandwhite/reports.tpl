{include file="header.tpl" }

<h3>Reports</h3>
<ul>
{nocache}
    {section name=idx loop=$reports}
        <li style="font: 20pt Arial;"><a href="showreport.php?rid={$reports[idx].rid}">{$reports[idx].title}</a></li>
        {sectionelse}
        <li>No Reports Found</li>
    {/section}
{/nocache}
</ul>

{include file="footer.tpl"}