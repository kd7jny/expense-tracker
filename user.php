<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/24/2018
 * Time: 8:41 AM
 */
require_once "startup.php";

class user{
    private $db;
    private $smarty;

    public function __construct($smarty, $db)
    {
        $this->db = $db;
        $this->smarty = $smarty;

    }

    public function updateUser(){
        $errors = [];

        $userid = $_POST['userid'];
        $name = trim($_POST['name']);
        $email = trim($_POST['email']);
        $oldpass = trim($_POST['oldpassword']);
        $newpass = trim($_POST['newpassword']);
        $confpass = trim($_POST['confirmpassword']);


        if(!$name) { $errors[] = 'No Update: Name Field was blank'; }
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) { $errors[] = "No Update: Email is not a valid format"; }

        if(count($errors) == 0 ) {
            $sql = "update user set name='$name', email='$email' where userid = $userid";
            $stmt = $this->db->prepare($sql);
            $stmt->execute();
        }
        if($oldpass):
            $sql = "select * from user where userid=$userid";
            $stmt = $this->db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $passwordhash = $data[0]['passwordhash'];
            if($newpass == $confpass && $newpass):
                if(password_verify($oldpass,$passwordhash)):
                    $newhash = password_hash($newpass,PASSWORD_DEFAULT);
                    $sql ="update user set passwordhash='$newhash' where userid=$userid";
                    $stmt = $this->db->prepare($sql);
                    $stmt->execute();
                    $errors[] = "Password Updated";
                else:
                    $errors[] = 'Bad Password!';
                endif;
            else:
                $errors[] = "New Passwords Do not Match or are Blank";
            endif;

        endif;

        $sql = "select * from user where userid=$userid";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $_SESSION['user'] = $data[0];

        $this->displayForm($errors);
    }


    public function displayForm($errors = [])
    {
        $sql = "select * from user where userid=" . $_SESSION['user']['userid'];
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->smarty->assign('menu', 'user');
        $this->smarty->assign('loop', $data);
        $this->smarty->assign('errors', $errors);
        $this->smarty->display('user.tpl');
    }


}

$user = new user($smarty, $db);


if(count($_POST)):
    $user->updateUser();
else:
    $user->displayForm();
endif;