<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/24/2018
 * Time: 10:04 AM
 */
require_once "Smarty.class.php";
class budgetSmarty extends Smarty
{
    function __construct()
    {
        $baseDir = __DIR__ . "/..";

        parent::__construct();

        $this->setTemplateDir($baseDir . '/templates/blackandwhite/');
        $this->setCompileDir($baseDir . '/templates_cache/');
        $this->setConfigDir($baseDir . '/configs/');
        $this->setCacheDir($baseDir . '/cache/');
        $this->caching = Smarty::CACHING_LIFETIME_CURRENT;
        //$this->assign('app_name', 'Guest Book');
    }

}