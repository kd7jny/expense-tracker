<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 6/1/2018
 * Time: 9:05 AM
 */
$dbfile  = 'budget1.sqlite';
if(file_exists($dbfile)) {
    unlink($dbfile);
}
$db = new PDO('sqlite:'.$dbfile);

$sql[] = "CREATE TABLE \"categories\" ( `cid` INTEGER PRIMARY KEY AUTOINCREMENT, `userid` INTEGER NOT NULL, `category` TEXT NOT NULL, `notes` TEXT, `amount` REAL NOT NULL DEFAULT 0.0, `shared` INTEGER NOT NULL DEFAULT 0, `active` INTEGER NOT NULL DEFAULT 1 )";
$sql[] = "CREATE TABLE \"transactions\" ( `tid` INTEGER PRIMARY KEY AUTOINCREMENT, `cid` INTEGER NOT NULL, `userid` INTEGER NOT NULL, `tdate` INTEGER, `description` TEXT, `amount` REAL, `notes` TEXT )";
$sql[] = "CREATE TABLE \"user\" ( `userid` INTEGER PRIMARY KEY AUTOINCREMENT, `email` TEXT NOT NULL UNIQUE, `passwordhash` TEXT NOT NULL, `name` TEXT )";
$sql[] = "CREATE VIEW v_transactions as select t.*, datetime(t.tdate,'unixepoch') as realdate, c.category, c.amount as budget from transactions t left join categories c on t.cid = c.cid";


$email = 'kd7jny@gmail.com';
$name = 'Jeff';
$password = 'link59sdf';
$password = password_hash($password,PASSWORD_DEFAULT);

$sql[] = "insert into user(email, name, passwordhash) values ('$email', '$name', '$password')";

$email = 'blcross@mail.com';
$name = 'Barbara';
$password = 'password';
$password = password_hash($password,PASSWORD_DEFAULT);

$sql[] = "insert into user(email, name, passwordhash) values ('$email', '$name', '$password')";

$uid = 1;
$categories = "House,Cox,Electric,Phone,Water,Netflix,Grocery,Restraunts,Car Insurance,Gas,Clothing,Personal,Credit Card,Student Loan";
foreach(explode(',', $categories) as $cat){
    $sql[] = "insert into categories(userid,category) values ($uid,'$cat')";
}




foreach($sql as $isql){
    file_put_contents('output.sql', $isql."\r\n\r\n", FILE_APPEND);
    $stmt = $db->prepare($isql);
    $stmt->execute();

}