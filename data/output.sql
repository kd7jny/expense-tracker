CREATE TABLE "categories" ( `cid` INTEGER PRIMARY KEY AUTOINCREMENT, `userid` INTEGER NOT NULL, `category` TEXT NOT NULL, `notes` TEXT, `amount` REAL NOT NULL DEFAULT 0.0, `shared` INTEGER NOT NULL DEFAULT 0, `active` INTEGER NOT NULL DEFAULT 1 )

CREATE TABLE "transactions" ( `tid` INTEGER PRIMARY KEY AUTOINCREMENT, `cid` INTEGER NOT NULL, `userid` INTEGER NOT NULL, `tdate` INTEGER, `description` TEXT, `amount` REAL, `notes` TEXT )

CREATE TABLE "user" ( `userid` INTEGER PRIMARY KEY AUTOINCREMENT, `email` TEXT NOT NULL UNIQUE, `passwordhash` TEXT NOT NULL, `name` TEXT )

CREATE VIEW v_transactions as select t.*, datetime(t.tdate,'unixepoch') as realdate, c.category, c.amount as budget from transactions t left join categories c on t.cid = c.cid

insert into user(email, name, passwordhash) values ('kd7jny@gmail.com', 'Jeff', '$2y$10$vyJo6DZC1ZE4VcYLi1PKW.z8Jw7wHdMDetmBk/qz1iHGBKf0Pbh2.')

insert into user(email, name, passwordhash) values ('blcross@mail.com', 'Barbara', '$2y$10$e9KoKqdfa0gbtvw/8Gzf6eHiY50J3BegU7lvM44WOki/J1joYUWra')

insert into categories(userid,category) values (1,'House')

insert into categories(userid,category) values (1,'Cox')

insert into categories(userid,category) values (1,'Electric')

insert into categories(userid,category) values (1,'Phone')

insert into categories(userid,category) values (1,'Water')

insert into categories(userid,category) values (1,'Netflix')

insert into categories(userid,category) values (1,'Grocery')

insert into categories(userid,category) values (1,'Restraunts')

insert into categories(userid,category) values (1,'Car Insurance')

insert into categories(userid,category) values (1,'Gas')

insert into categories(userid,category) values (1,'Clothing')

insert into categories(userid,category) values (1,'Personal')

insert into categories(userid,category) values (1,'Credit Card')

insert into categories(userid,category) values (1,'Student Loan')

