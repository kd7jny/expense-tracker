<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/24/2018
 * Time: 8:41 AM
 */

require_once "startup.php";

$sql = "select * from user";
$stmt = $db->prepare($sql);
$stmt->execute();
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
$active_uid = $_SESSION['active_uid'];

$uid =  $_SESSION['user']['userid'];
$sql = "select cid, category, shared from categories where active=1 and (userid = $active_uid or shared = 1) order by category";
$stmt = $db->prepare($sql);
$stmt->execute();

$active = $stmt->fetchAll(PDO::FETCH_ASSOC);


$sql = "select * from v_transactions where userid = $active_uid order by tdate desc limit 100";
$stmt = $db->prepare($sql);
$stmt->execute();

$transactions = $stmt->fetchAll(PDO::FETCH_ASSOC);
//echo "<pre>";
//var_dump($transactions);
//die();


$date = date('Y-m-d');
$smarty->assign('uid', $_SESSION['user']['userid']);

$smarty->assign('transactions', $transactions);
$smarty->assign('category', $active);
$smarty->assign('date', $date);
$smarty->assign('amount',0);
$smarty->assign('menu','home');
$smarty->assign('myuser', $data);
$smarty->display('index.tpl');
