<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/24/2018
 * Time: 8:41 AM
 */
require_once "startup.php";
$data = [
    'category' => '',
    'amount' => 0,
    'notes' => '',
];

if(count($_POST)):
    $cat = makesafesqlstring($_POST['category']);
    $amt = $_POST['amount'];
    $notes = makesafesqlstring($_POST['notes']);
    $shared = isset($_POST['shared']) ? $_POST['shared'] : 0;
 //   $uid = $_SESSION['user']['userid'];
    $uid = $_SESSION['active_uid'];

    $sql = "insert into categories(userid,category,notes,amount,shared, active) 
            VALUES 
            ($uid, '$cat', '$notes', $amt, $shared, 1)
            ";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    header('Location: category.php');
endif;

$errors = '';

$smarty->assign('menu','cat');
$smarty->assign('data', $data);
$smarty->assign('cid',$cid);
$smarty->assign('errors',$errors);
$smarty->display('catadd.tpl');
