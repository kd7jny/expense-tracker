<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/30/2018
 * Time: 3:29 PM
 */

session_start();
require_once "dbconf.php";
require_once("libs/budgetSmarty.php");
$smarty = new budgetSmarty();

$currentpage = basename($_SERVER['PHP_SELF']);


if(isset($_COOKIE['userid'])):
    if(!isset($_SESSION['user'])):

        $sql = "select * from user where userid=".$_COOKIE['userid'];
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($user) == 1):
            $_SESSION['user'] = $user[0];
            $_SESSION['active_uid'] = $user[0]['userid'];

        endif;
    endif;
    setcookie('userid',$_SESSION['user']['userid'], time() + (60*60*24*7));
endif;


if(isset($_SESSION['user']) || $currentpage == 'login.php'):
    $smarty->assign('session', $_SESSION);
    $smarty->assign('active_uid',  $_SESSION['active_uid']);
else:
        header('Location: login.php');
endif;


$userlist = [];
$sql = "select userid,name from user order by name";
$stmt = $db->prepare($sql);
$stmt->execute();
$userlist = $stmt->fetchAll(PDO::FETCH_ASSOC);
$smarty->assign('users', $userlist);
$smarty->assign('currentpage', $currentpage);

// defined MONEY constant for STRING REPLACEMENTS
const MONEY = '$%.2f';



function unixtodate($unixtime){
    return date('Y-m-d', $unixtime);
}

function makesafesqlstring($str){
    $str = trim($str);
    $str = str_replace("'", "''", $str);
//    $str = sqlite_escape_string ($str);
    return $str;
}