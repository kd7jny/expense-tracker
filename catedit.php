<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/24/2018
 * Time: 8:41 AM
 */
require_once "startup.php";


$cid = $_GET['cid'];

if(count($_POST)):

    $cat = makesafesqlstring($_POST['category']);
    $amt = $_POST['amount'];
    $notes = makesafesqlstring($_POST['notes']);
    $shared = isset($_POST['shared']) ? $_POST['shared'] : 0;
    $active = isset($_POST['active']) ? $_POST['active'] : 0;
    $uid = $_SESSION['user']['userid'];

    $sql = "update categories 
            set category = '$cat',
            notes = '$notes',
            amount = $amt,
            shared = $shared, 
            active = $active  
            where cid = $cid
            ";

    $stmt = $db->prepare($sql);
    $stmt->execute();
    header('Location: category.php');


endif;




$sql = "select * from categories where cid=$cid";
$stmt = $db->prepare($sql);
$stmt->execute();
//$data = [];
//if($row = $stmt->fetch(PDO::FETCH_ASSOC))
//{
//    $data = $row;
//};
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
$errors = '';

$smarty->assign('menu','cat');
$smarty->assign('loop', $data);
$smarty->assign('cid',$cid);
$smarty->assign('errors',$errors);
$smarty->display('catedit.tpl');
