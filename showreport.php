<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/24/2018
 * Time: 8:41 AM
 */
require_once "startup.php";

class showreport{
    public $db;
    public $debug;
    public $smarty;

    public function __construct($db,$smarty)
    {
        $this->db = $db;
        $this->smarty = $smarty;
        $this->debug = false;
    }

    public function main(){
        $rid = $_GET['rid'];

        $report = include "reports/".$rid .".php";
        $title = $report['title'];
        $sql = $report['sql'];
        $params =  isset($report['params']) ? $report['params'] : [];
        $formats = isset($report['formats']) ? array_change_key_case($report['formats'], CASE_LOWER) : [];
        $disabled = isset($report['disabled']) ? $report['disabled'] : false;
        $this->debug = isset($report['debug']) ? $report['debug'] : false;
        $table = '';

        if(!$disabled):
            $form = $this->generateForm($params, $title);

            if((count($params) && count($_POST)) || !count($params)):
                $table = "";
                if(!is_array($sql)):
                    $newsql[] = $sql;
                else:
                    $newsql = $sql;
                endif;
                $tablecount = 0;
                foreach($newsql as $subtitle=>$sqlstmt ) {
                    $tablecount++;
                    if(!is_int($subtitle)):
                        $table.="<h4>$subtitle</h4>";
                    endif;
                    $table .= $this->generatetable($sqlstmt, $params,$formats,$tablecount) . "";
                }
            else:
                $table = '';
            endif;
        else:
            $form = "";
            $table = "<h4>Report has been Disabled</h4>";
        endif;



//        if($this->debug) { die();}


        $this->smarty->assign('form', $form);
        $this->smarty->assign('rid',$rid);
        $this->smarty->assign('menu','report');
        $this->smarty->assign('table',$table);
        $this->smarty->assign('title', $title);
        $this->smarty->assign('count', count($newsql));
        $this->smarty->display('basereport.tpl');
    }

    private function generatetable($sql,$params, $fieldformats,$tableid){


        $table ="<table class='reportdata' id='table$tableid'>";

        if(count($params)):
            foreach($params as $field){
                $id = $field['id'];

                $str = "[[".$id."]]";
                $newvalue = isset($_POST[$id]) ? $_POST[$id] : '';
                if($field['type'] == 'date') {
                    $newvalue = strtotime($newvalue);
                }
                $sql = str_replace($str,$newvalue,$sql);
            }
        endif;

        $sql = str_replace('[[uid]]', $_SESSION['active_uid'],$sql);
//        if($this->debug == true)
//        {
//            echo "<pre>".$sql."\r\n</pre>";
//
//        }


        try {
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    //    $table .="<tr><th colspan='20'>$title Report</th></tr>";
        if(count($data)):
            $header = array_keys($data[0]);
            $table.="<tr>";
            foreach($header as $item){
                $table.="<th>".ucwords($item)."</th>";
            }
            $table.="</tr>";

            foreach($data as $row){
                $table.="<tr>";
                foreach($row as $key=>$cell){
                    $key = strtolower($key);
                    if(isset($fieldformats[$key]) && $cell != ''):
                        $cell = sprintf($fieldformats[$key], $cell);
                        endif;

                    $table.="<td>$cell</td>";
                }
                $table.="</tr>";
            }

        endif;
        }catch (PDOException $e){
            $table.="<tr><td colspan=20>There was an Error</td></tr>";
            if($this->debug):
                $table.="<tr><td colspan=20>".$e->getMessage()."</td></tr>";
                $table.="<tr><td colspan=20><pre>".$sql."</pre></td></tr>";
            endif;
        }

        $table.="</table>";
        $table.="<a href='#' onclick=\"fnExcelReport('table$tableid');\" class='exportlink'><i class='fas fa-file-export'> Export to File</i></a><br /><br />";
        return $table;
    }


    private function generateForm($params, $title){
        $form = '';

        if(count($params)):

            $form.="<form action=showreport.php?rid=".$_GET['rid']." method='POST'  class=\"form_settings\" style='margin: 0 0 0 0;'>";
            $form.="<table style='font: 75% Arial;'>";
            $form.="<tr><th colspan=2>$title Parameters</th></tr>";
            foreach($params as $field):
                $form.= $this->generateField($field)."<br>";
            endforeach;
            $form.="</table><input type=submit value=Go class=mysubmit style='font: 75% Arial;'></form><br><hr><br>";
        endif;

        return $form;
    }


    private function generateField($field){

        $months = [
            'Jan','Feb','Mar','Apr',"May","Jun", "Jul", 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
        ];

        $uid = $_SESSION['active_uid'];
        $input = '';
        $type = isset($field['type']) ? $field['type'] : 'text';
        $title = trim($field['title']);
        $id = $field['id'];
        $default = isset($field['default']) ? $field['default'] : '';
        $value = isset($_POST[$id]) ?  $_POST[$id] : $default;
        $uid = $_SESSION['active_uid'];

        switch($type){
            case 'text':
                $input = "<tr><th>$title</th><td><input type=text name='$id' value='$value'></td></tr>";
                break;
            case 'date':
                $input = "<tr><th>$title</th><td><input type=date name='$id' value='$value'></td></tr>";
                break;
            case 'amount':
                $value = $value == '' ? 0.0 : floatval($value);
                $input = "<tr><th>$title</th><td>$<input type=number step=0.01 name='$id' value='$value' size='8'></td></tr>";
                break;
            case 'real':
                $value = $value == '' ? 0.0 : floatval($value);
                $input = "<tr><th>$title</th><td><input type=number step=0.01 name='$id' value='$value'></td></tr>";
                break;
            case 'integer':
                $value = $value == '' ? 0 : intval($value);
                $input = "<tr><th>$title</th><td><input type=number step=1 name='$id' value='$value'></td></tr>";
                break;
            case 'year':
                $value = $value == '' ? date('Y') : intval($value);
                $input = "<tr><th>$title</th><td><select name='$id'>";

                for($x=date('Y')-5;$x<= date('Y')+10;$x++){
                    $selected = $x == $value ? 'selected=selected' : '';
                    $input.="<option value=$x $selected>$x</option>";
                }

                $input.= "</select></td></tr>";
                break;
            case 'month':
                $value = $value == '' ? date('m') : intval($value);
                $value = str_pad($value,2, '0', STR_PAD_LEFT);
                $input = "<tr><th>$title</th><td><select name='$id'>";

                foreach($months as $key => $month){
                    $x = $key + 1;
                    $x = str_pad($x,2, '0', STR_PAD_LEFT);
                    $selected = $x == $value ? 'selected=selected' : '';
                    $monthname =
                    $input.="<option value='$x' $selected>$month</option>";
                }

                $input.= "</select></td></tr>";
                break;
            case 'category':
                $input = "<tr><th>$title</th><td><select name='$id'><option value='0'></option>";
                $sql = "select cid, category, shared from categories where (userid = $uid or shared = 1) and active = 1 order by category";
                $stmt = $this->db->prepare($sql);
                $stmt->execute();
                $cats = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($cats as $item){

                    $selected = $item['cid'] == $value ? 'selected=selected' : '';
                    $shared = $item['shared'] == 1 ? " [Shared]" : '';
                    $input.="<option value='".$item['cid']."' $selected>".$item['category'].$shared."</option>";
                }

                $input.= "</select></td></tr>";
                break;

            case 'user':
                $value = isset($_POST[$id]) ?  $_POST[$id] : $_SESSION['active_uid'];
                $input = "<tr><th>$title</th><td><select name='$id'>";
                $sql = "select userid, name from user order by name";
                $stmt = $this->db->prepare($sql);
                $stmt->execute();
                $cats = $stmt->fetchAll(PDO::FETCH_ASSOC);

                foreach($cats as $item){

                    $selected = $item['userid'] == $value ? 'selected=selected' : '';
                    $input.="<option value='".$item['userid']."' $selected>".$item['name']."</option>";
                }

                $input.= "</select></td></tr>";
                break;

        }

        return $input;
    }
}


if( $_GET['rid'] == ''):
    header('Location: reports.php');
endif;

$page = new showreport($db,$smarty);
$page->main();

