<?php
/**
 * Created by PhpStorm.
 * User: nblade
 * Date: 5/31/18
 * Time: 8:14 PM
 */

$array = [
    'sql' => 'select * from test where [f1] = [f2]',
    'params' =>
        [
            [
                'name'=> 'f1',
                'type'=> 'options',
                'options'=> 'Fred,Frank,James'
            ],
            [
                'name'=> 'f2',
                'type'=> 'text',
                'default'=> 'Judy'
            ],

        ]
    ];

echo "<pre>";
echo json_encode($array);