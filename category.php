<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/24/2018
 * Time: 8:41 AM
 */
require_once "startup.php";

//$uid =  $_SESSION['user']['userid'];
$uid = $_SESSION['active_uid'];

$sql = "select * from categories where active=1 and (userid = $uid or shared = 1) order by category";
$stmt = $db->prepare($sql);
$stmt->execute();

$active = $stmt->fetchAll(PDO::FETCH_ASSOC);

$sql = "select * from categories where active=0 and (userid = $uid or shared = 1) order by category";
$stmt = $db->prepare($sql);
$stmt->execute();

$inactive = $stmt->fetchAll(PDO::FETCH_ASSOC);

$smarty->assign('menu','cat');
$smarty->assign('activeloop', $active);
$smarty->assign('inactiveloop', $inactive);
$smarty->display('category.tpl');
