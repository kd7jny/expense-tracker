<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/24/2018
 * Time: 8:41 AM
 */

require_once "startup.php";


if(count($_POST)):

    $tid = $_GET['tid'];
    $cid = $_POST['category'];
    $tdate = strtotime($_POST['tdate']);
    $description = makesafesqlstring($_POST['description']);
    $notes = makesafesqlstring($_POST['notes']);
    $amount = floatval($_POST['amount']);
    $delete = isset($_POST['delete']) ? $_POST['delete'] : 0;
    $active_uid = $_SESSION['active_uid'];

    if($delete == 1) :
        $sql = "delete from transactions where tid=$tid";
    else:
        $sql = "update transactions 
                set cid=$cid,
                tdate=$tdate,
                amount=$amount,
                description='$description',
                notes='$notes'
                where tid=$tid";
    endif;
    $stmt = $db->prepare($sql);
    $stmt->execute();
    header('Location: index.php');

endif;



//$uid =  $_SESSION['user']['userid'];
$uid = $_SESSION['active_uid'];
$sql = "select cid, category, shared from categories where active=1 and (userid = $uid or shared = 1) order by category";
$stmt = $db->prepare($sql);
$stmt->execute();

$active = $stmt->fetchAll(PDO::FETCH_ASSOC);

$tid  = $_GET['tid'];
$sql = "select * from transactions where tid=$tid";
$stmt = $db->prepare($sql);
$stmt->execute();

$trans = $stmt->fetchAll(PDO::FETCH_ASSOC);
$cat  = $trans[0]['cid'];
$date = unixtodate($trans[0]['tdate']);
$description = $trans[0]['description'];
$notes = $trans[0]['notes'];
$amount = $trans[0]['amount'];

$smarty->assign('category', $active);
$smarty->assign('date', $date);
$smarty->assign('cat',$cat);
$smarty->assign('amount',$amount);
$smarty->assign('description',$description);
$smarty->assign('notes',$notes);
$smarty->assign('tid',$tid);
$smarty->assign('menu','home');
//$smarty->assign('myuser', $data);
$smarty->display('edittransaction.tpl');
