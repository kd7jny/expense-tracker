<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 5/24/2018
 * Time: 8:41 AM
 */

require_once "startup.php";


$data = [
    'email' => '',
];
$errors = '';

if($_POST) {

    $email = trim($_POST['email']);
    $pass = trim($_POST['password']);

    $sql = "select * from user where email='$email'";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if(count($user) == 1):
        $user = $user[0];
        $verify = password_verify($pass, $user['passwordhash']);
        if($verify):
            $errors = "Password verified";
            $_SESSION['user'] = $user;
            $_SESSION['active_uid'] = $user['userid'];
            setcookie('userid',$user['userid'], time() + (60*60*24*7));
            header("Location: index.php");
        else:
            $errors = "Username or Password Bad";

        endif;
    else:
        $errors = "Username or Password Bad";
    endif;

}

$smarty->assign('menu','user');

$smarty->assign('data', $data);
$smarty->assign('errors', $errors);
$smarty->display('login.tpl');
