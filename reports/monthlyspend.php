<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 6/4/2018
 * Time: 6:56 AM
 */
return
[
    'title' => 'Monthly Spend Report',
    'sql' => [
              'Budget Totals' => 'select 
                c.category as Category, 
                    c.amount as Budget,
                    sum(t.amount) as Total,
                    c.amount - sum(t.amount) as \'Difference\' 
                from 
                    (select * from categories where active=1 and (userid=[[u1]] or shared=1) ) c left join
                    (select * from transactions  
                where 
                    userid = [[u1]]
                    and strftime(\'%m\',datetime(tdate,\'unixepoch\')) = \'[[d1]]\'
                    and strftime(\'%Y\',datetime(tdate,\'unixepoch\')) = \'[[d2]]\'
                    ) t
                    on t.cid = c.cid 
                group by c.category
                order by c.category
      ',

        'Monthly Totals' => 'select budget as \'Budget Total\',total as \'Transaction Total\',(budget - total) as \'Difference\'
                            from
                            (select sum(amount) as \'budget\' from (select cid,max(amount) as amount from categories where active=1 and (userid=[[u1]] or shared=1) group by cid))
                            inner join
                            (select sum(amount) as total from transactions where userid = [[u1]] and strftime(\'%m\',datetime(tdate,\'unixepoch\')) = \'[[d1]]\' and strftime(\'%Y\',datetime(tdate,\'unixepoch\')) = \'[[d2]]\')',



        'Monthly Transactions' => 'select 
              strftime(\'%m/%d/%Y\',datetime(tdate,\'unixepoch\')) as `date`,
              category, 
              description, 
              notes,
              amount
            from 
              v_transactions 
            where 
              userid = [[u1]] 
                and strftime(\'%m\',datetime(tdate,\'unixepoch\')) = \'[[d1]]\'
                and strftime(\'%Y\',datetime(tdate,\'unixepoch\')) = \'[[d2]]\'
            order by tdate'
    ],
    'formats' =>
    [
        'amount' => MONEY,
        'budget' => MONEY,
        'total' => MONEY,
        'difference' => MONEY,
    ],
    'params' => [
        [ 'id'=>'d1',
            'title' => 'Month',
            'type' => 'month',
        ],
        [ 'id'=>'d2',
            'title' => 'Year',
            'type' => 'year',
        ],
        [
            'id' => 'u1',
            'title' => 'User',
            'type' => 'user'
        ],
    ],
    'debug' => true,
];


/*

select strftime('%m/%d/%Y',datetime(tdate,'unixepoch')) as `date`,
category, description, printf("$%.2f", amount) as amount
from v_transactions where userid = [[uid]] and (tdate between [[d1]] and [[d2]]) order by tdate

 */