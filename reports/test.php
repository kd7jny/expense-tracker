<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 6/4/2018
 * Time: 6:56 AM
 */
return
[
    'title' => 'Test',
    'sql' => [
       'users' => 'select * from user',
       'categories' =>  'select * from categories',
    ],
    'formats' =>
    [
        'amount' => MONEY,
        'budget' => MONEY,
        'total' => MONEY,
        'difference' => MONEY,
    ],
    'debug' => false,
    'disabled' => true,
];