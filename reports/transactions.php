<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 6/4/2018
 * Time: 6:56 AM
 */
return
[
    'title' => 'Transactions Between Dates',
    'sql' => '
select 
  strftime(\'%m/%d/%Y\',datetime(tdate,\'unixepoch\')) as `date`,
  category, 
  description, 
  notes,
  amount
from 
  v_transactions 
where 
  userid = [[u1]] 
  and (tdate between [[d1]] and [[d2]]) 
order by tdate',
    'params' => [
        [ 'id'=>'d1',
            'title' => 'Start Date',
            'type' => 'date',
            'default' => date("Y-m-01"),
        ],
        [ 'id'=>'d2',
            'title' => 'End Date',
            'type' => 'date',
            'default' => date("Y-m-t"),
        ],
        [
            'id' => 'u1',
            'title' => 'User',
            'type' => 'user'
        ],
    ],
 'formats' =>
    [
        'amount' => MONEY,
        'budget' => MONEY,
        'total' => MONEY,
        'difference' => MONEY,
    ],
    'debug' => true,
];
