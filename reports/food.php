<?php
/**
 * Created by PhpStorm.
 * User: Jeffrey Brissette
 * Date: 6/4/2018
 * Time: 6:56 AM
 */
return
[
    'title' => 'Grocery and Restraunts',
    'sql' => [
        'Montly Transactions' =>  '
        select 
          strftime(\'%m/%d/%Y\',datetime(t.tdate,\'unixepoch\')) as `date`,
          t.category as category, 
          t.description as description, 
          t.notes as notes,
          u.name as name,
          t.amount as Amount
        from 
          v_transactions t left join user u on t.userid = u.userid
        where 
          strftime(\'%m\',datetime(tdate,\'unixepoch\')) = \'[[d1]]\'
          and strftime(\'%Y\',datetime(tdate,\'unixepoch\')) = \'[[d2]]\'
          and category in ("Grocery", "Restraunts")
         order by date desc, category',

        'Montly Totals' => '
         select u.name as user,
            c.category as category,
            "" as Budget,
            sum(t.amount) as "Total Spent",
            "" as "Difference"
        from categories c left join v_transactions t on c.cid = t.cid left join user u on t.userid = u.userid
        where c.category in ("Restraunts", "Grocery")
        and strftime(\'%m\',datetime(tdate,\'unixepoch\')) = \'[[d1]]\'
        and strftime(\'%Y\',datetime(tdate,\'unixepoch\')) = \'[[d2]]\'
        group by u.name, c.category

        UNION

        select "*** Totals ***",
            c.category,
            c.amount as Budget,
            sum(t.amount) as "Total Spent",
            c.amount - sum(t.amount) as "Difference"
        from categories c left join v_transactions t on c.cid = t.cid left join user u on t.userid = u.userid
        where c.category in ("Restraunts", "Grocery")
          and strftime(\'%m\',datetime(tdate,\'unixepoch\')) = \'[[d1]]\'
          and strftime(\'%Y\',datetime(tdate,\'unixepoch\')) = \'[[d2]]\'
        group by c.category
       

        
        union
              
                select "*** Totals ***",
            "*** Totals ***",
            (select sum(amount) from categories where category in ("Restraunts", "Grocery"))  as Budget,
            sum(t.amount) as "Total Spent",
            (select sum(amount) from categories where category in ("Restraunts", "Grocery")) - sum(t.amount) as "Difference"
        from categories c left join v_transactions t on c.cid = t.cid left join user u on t.userid = u.userid
          and strftime(\'%m\',datetime(tdate,\'unixepoch\')) = \'[[d1]]\'
          and strftime(\'%Y\',datetime(tdate,\'unixepoch\')) = \'[[d2]]\'       
        
         order by name desc , c.category desc
        ',

    ],
    'formats' =>
        [
            'Amount' => MONEY,
            'Budget' => MONEY,
            'Total Spent' => MONEY,
            'Total' => MONEY,
            'Difference' => MONEY,
        ],
    'params' => [
        [ 'id'=>'d1',
            'title' => 'Month',
            'type' => 'month',
        ],
        [ 'id'=>'d2',
            'title' => 'Year',
            'type' => 'year',
        ],
    ]
    ,
    'debug' => false,
];